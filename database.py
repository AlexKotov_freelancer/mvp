from os import name
import sqlite3
import json


class Database:
    def __init__(self) -> None:
        self.con = sqlite3.connect('answer.db')
        self.cur = self.con.cursor()
        
    
    # abstract methods
    def create_table(self, name: str = None, fields: str = None):
        # write new table
        self.cur.execute(f'''CREATE TABLE IF NOT EXISTS {name} ({fields})''')
        # save new table
        self.con.commit()
        # close current request
        #self.con.close()
        
    # work methods
    def main(self):
        self.create_table(name='items_list', fields="""name TEXT UNIQUE, link TEXT UNIQUE, current_price INT, last_update text, sizes TEXT""")
        self.create_table(name='price_history', fields="""link TEXT, price INT, time text""")
        
    def update_items_list(self, data):
        print(data)
        sql = f"""UPDATE items_list SET current_price = '{data[2]}', last_update = '{data[3]}', sizes = '{str(data[-1])}' WHERE link = '{data[1]}'"""
        self.cur.executescript(sql)    
        self.con.commit()
        
    def write_data_history(self, data):
        # price history write
        self.cur.execute(f"""INSERT INTO price_history VALUES ('{data['link']}','{data['current_price']}', '{data['time']}')""")
        self.con.commit()    
    
        
    def write_on_items_list(self, data: dict = None):
        if data != None:
            # create table
            self.main()
            
            name = data['link'].split('/p/')[-1]
            quantity = []
              
              
            sizes = ', '.join(data['sizes'])   
            # write data
            try:
            
                # write Unique data 
                self.cur.execute(f"""INSERT INTO items_list VALUES ('{name}','{data['link']}','{data['current_price']}','{data['time']}','{sizes}')""")
                self.con.commit()
                
                # price history write
                self.write_data_history(data)

            # if data not Unique we update data and write price in price history
            except sqlite3.IntegrityError:
                self.update_items_list((str(name), data['link'], data['current_price'], data['time'], sizes))
                # price history write
                self.write_data_history(data)

            
        else:
            print("data == None")
    
# if __name__ == "__main__":
#     Database().main()