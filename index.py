import requests
from bs4 import BeautifulSoup as bs
import re
import json
import datetime
    
class Scrape_page:
    # init
    def __init__(self, link: str = None):
        # url request
        self.querry = link
        self.data = {}
        
        # output var's
        self.link: str = None
        self.time: str = None
        self.sizes:str = None
        self.price:int = None
        
        # start methods   
        self.get_post_data()
        #self.output()
    
    # methods
    def get_post_data(self):
        # good header
        header = {
            "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36"
        }
        # get response with header
        data = requests.get(self.querry, headers=header)
        # if we gett data
        if data.status_code == 200:
            # converting respons to json
            self.convert_respons_to_json_as_bs(data.text)
        else:
            print(data.status_code) 
        
    def convert_respons_to_json_as_bs(self,data):
        # conver response to bs element
        soup = bs(data, 'html.parser') 
        # getting all script files
        scripts = soup.find_all("script")
        # to find the most needed script tag
        for script in scripts:
            # we know what our json file very big > 300000 char
            if len(str(script)) >= 200000:
                # split script tag's
                data = str(script).split("""<script>
                    window.__PRELOADED_STATE__ = """)[1].split("""
                </script>""")[0]
                
                # load json
                data = json.loads(data)
                # prettify json
                #data = json.dumps(data, indent=4)
                # save json in class
                self.data = data        


            
    
    def output(self) -> dict:
        self.link = self.querry
        self.price = self.data['productCard']['product']['cardDetails']['price']
        data = []
        for i in self.data['productCard']['product']['cardDetails']['sizes']:
            data.append(str(f"{i['name']} = {i['variation']['quantity']}"))
        self.sizes = data
        self.time = datetime.datetime.timestamp(datetime.datetime.utcnow())    
        
        return {"link": self.link, "current_price": self.price, "sizes": self.sizes, "time": self.time}




if __name__ == "__main__":
    Scrape_page('https://answear.ua/p/adidas-performance-cherevyky-terrex-voyager-21-413909')