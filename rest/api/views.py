from django.shortcuts import get_object_or_404, render
from rest_framework.viewsets import ModelViewSet
from .models import Price_history, Item_list
from .index import *
from .serializers import *
import time
from itertools import chain
from rest_framework.response import Response
from rest_framework.decorators import api_view

@api_view(['GET'])
def outp(request):
    req_data = request.query_params.dict()
    print(str(req_data['apikey']))
    if req_data['apikey'] == '417195122c854166a1e2fcbe061add3c' or req_data['apikey'] == 'fd26eb49716b4dd48252a0ab0dd88ba1':
        
    # adidas-performance-cherevyky-terrex-voyager-21-413909
        sp = Scrape_page(str(req_data['name']).split('/')[-1])
        data = sp.output() 
        # data dict example   
        
        if data == 'KeyError':
            return Response({"web-service not respond, please try later"})
        
        
        else:
            # creating dictioanary
            datas = {
                "link":          data['link'],
                "current_price": data['current_price'],
                "last_update":   data['time'],
                "sizes":         ', '.join(data['sizes']),
            }
            
            
            # update queryset and rewrite item data
            item = Item_list.objects.filter(link__icontains = f"https://answear.ua/ru/p/{str(req_data['name']).split('/')[-1]}")
            item.update(**datas)
            # create if have this row
            Item_list.objects.update_or_create(**datas)
            
            # create new row for price history
            history_data = {
                "link":data['link'],
                "price": data['current_price'],
                "timestamp": data['time']
            }
            # creating new history row
            Price_history.objects.create(**history_data)
            
            
            # render
            history = Price_history.objects.filter(link__icontains = f"https://answear.ua/ru/p/{str(req_data['name']).split('/')[-1]}")
            itemSeriale = ModelASerializer(item, many=True)
            historySeriale = ModelBSerializer(history, many=True)
            Resultmodel = dict()
            Resultmodel['current']= list(itemSeriale.data)[0]
            Resultmodel['history']= history_dict(historySeriale)
            return Response(Resultmodel)
    else:
        return Response({"Your apikey doesn't correct"})


#  function for getting history dict from queryObject
def history_dict(dictionary):
    history_object = {}
    for i in dictionary.data:
        history_object[i['timestamp']] = i['price']
        
    return history_object