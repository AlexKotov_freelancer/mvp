from rest_framework.serializers import ModelSerializer
from rest_framework import serializers 
from .models import *
from .index import *





class ModelASerializer(serializers.ModelSerializer):
    class Meta:
        model = Item_list
        fields = ('link', 'current_price', 'sizes')
        
class ModelBSerializer(serializers.ModelSerializer):
    class Meta:
        model = Price_history
        fields = ('price', 'timestamp')
