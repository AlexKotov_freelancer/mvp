from django.db import models

# Create your models here.
class Item_list(models.Model):
    link = models.CharField(max_length=500)
    current_price = models.IntegerField(blank=True)
    last_update = models.CharField(max_length=500)
    sizes = models.CharField(max_length=500)
    
    
class Price_history(models.Model):
    link = models.CharField(max_length=500)
    price = models.IntegerField(blank=True)
    timestamp = models.CharField(max_length=500)